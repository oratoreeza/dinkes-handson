<?php

namespace App\Http\Controllers;

use App\Services\RsudApiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use PDO;

class RsudApiController extends Controller
{
    public function __construct()
    {
        $this->rsService = new RsudApiService();
    }

    public function all(Request $request)
    {
        $filters = $request->validate([
            "kelurahan" => "string",
            "kecamatan" => "string",
            "kota_madya" => "string",
        ]);

        $rsCovid = Cache::rememberForever('rs_covid', function () {
            return $this->rsService->getRsCovid();
        });

        $rsDetails = Cache::rememberForever('rs_details', function () {
            return $this->rsService->getRsDetails();
        });

        $filteredRsCovid = $this->filterRs($filters, $rsCovid);

        $mergedData = $this->mapRs($filteredRsCovid, $rsDetails);

        return response()->json($mergedData);
    }

    private function mapRs(array $rsCovid, array $detailRs)
    {
        $rsCovidCollection = collect($rsCovid);
        $detailRsCollection = collect($detailRs);

        return $rsCovidCollection->map(function ($rsCovid) use ($detailRsCollection) {
            // $matchedDetail = $detailRsCollection->firstWhere('nama_rumah_sakit', $rsCovid->nama_rumah_sakit);
            $matchedDetail = $detailRsCollection->filter(function ($detailRs) use ($rsCovid) {
                return (false !== stristr($rsCovid->nama_rumah_sakit, $detailRs->nama_rumah_sakit))
                    && (strtolower($rsCovid->kelurahan) == strtolower($detailRs->kelurahan));
            })->first();
            $mappedRsCovid = (object)[];
            $mappedRsCovid->nama_rumah_sakit = $rsCovid->nama_rumah_sakit ?? null;
            $mappedRsCovid->jenis_rumah_sakit = $matchedDetail->jenis_rumah_sakit ?? null;
            $mappedRsCovid->alamat_rumah_sakit = $matchedDetail->alamat_rumah_sakit ?? null;
            $mappedRsCovid->kelurahan = $rsCovid->kelurahan ?? null;
            $mappedRsCovid->kecamatan = $rsCovid->kecamatan ?? null;
            $mappedRsCovid->kota_madya = $rsCovid->kota_madya ?? null;
            $mappedRsCovid->kode_pos = $matchedDetail->kode_pos ?? null;
            $mappedRsCovid->nomor_telepon = $matchedDetail->nomor_telepon ?? null;
            $mappedRsCovid->nomor_fax = $matchedDetail->nomor_fax ?? null;
            $mappedRsCovid->website = $matchedDetail->website ?? null;
            $mappedRsCovid->email = $matchedDetail->email ?? null;
            return $mappedRsCovid;
        });
    }

    private function filterRs($filters, $rsCovid)
    {
        if (count($filters) == 0) {
            return $rsCovid;
        }
        $filteredRs = [];
        $rsCovid = collect($rsCovid);
        foreach ($filters as $keyFilter => $valueFilter) {

            $filterResults = $rsCovid->where($keyFilter, $valueFilter);

            // $filterResults = $rsCovid->filter(function ($rsCov) use ($keyFilter, $valueFilter) {
            //     return $rsCov->$keyFilter == $valueFilter;
            // });

            foreach ($filterResults as $result) {
                $filteredRs[] = $result;
            }
        }

        return $filteredRs;
    }
}
